#!/usr/bin/env python
# _*_ coding: utf-8 _*_

"""
autolust.autolust
~~~~~~~~~~~~~~~~~

This is the application skeleton for the AutoLust project.
"""

from flask import Flask, render_template, request
from flask.ext.script import Manager
from flask.ext.sqlalchemy import SQLAlchemy
from config import DB_URL as DB_URL
import model

app = Flask(__name__)
manager = Manager(app)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
db = SQLAlchemy(app)


@app.route('/vehicle', methods=['GET', 'POST'])
def create_vehicle():
    if request.method == 'GET':
        return render_template('vehicle.html')
    elif request.method == 'POST':
        year = int(request.form['year'])
        #make = request.form['make']
        #model = request.form['model']
        #trim = request.form['trim']
        #image_url = request.form['image_url']
        #style = int(request.form['style'])
        #fuel_economy = int(request.form['fuel_economy'])
        #engine_type = int(request.form['engine_type'])
        #transmission = int(request.form['transmission'])

        # Create the vehicle object and add it to database
        vehicle = Vehicle(year = year)
        print year
       #db.session.add(vehicle)
        #db.session.commit()
        
        return render_template('vehicle.html')
    else:
        abort(401)

if __name__ == '__main__':
    manager.run()


