from autolust import db

class Vehicle(db.Model):
    __tablename__ = 'vehicles'
    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer)
    make = db.Column(db.String(20))
    model = db.Column(db.String(20))
    trim = db.Column(db.Integer)
    style = db.Column(db.Integer)
    fuel_economy = db.Column(db.Integer)
    engine_type = db.Column(db.Integer)
    transmission = db.Column(db.Integer)
    image_url = db.Column(db.Text)


