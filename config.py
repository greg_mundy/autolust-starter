#!/usr/bin/env python
# _*_ coding: utf-8 _*_

"""
autolust.config
~~~~~~~~~~~~~~~
Configuration parameters for the AutoLust project.

"""

import os


try:
    if os.environ['RACK_ENV'] == 'production':
        DB_URL = ""
    elif os.environ['RACK_ENV'] == 'test':
        DB_URL = ""
except:
    DB_URL = "mysql://autolust_dev:1autolust_dev1@autolust-dev-db.c0gmg8xami1q.us-east-1.rds.amazonaws.com/autolust_dev"

